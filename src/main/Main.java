package main;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import upload.UploadServer;
import utils.TrackerUtils;
import utils.Utils;
import download.Downloader;
import download.SharedFile;

public class Main {

	public static String peer_id = "";
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static int server_port = DEFAULT_SERVER_PORT;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static final String DEFAULT_TRACKERFILE_FOLDER = System.getProperty("user.home")+File.separator+".tracker_files";
	public static void main(String[] args) {
		int port = DEFAULT_SERVER_PORT;
		String trackerFile = null;
		if(args.length == 0 || args.length > 3){
			System.out.println("o endereço do tracker e a porta devem ser passados como argumentos");
			//System.out.println("Usage: quantashare <TRACKER> [port] [tracker file]");
			System.exit(1);
		}
		if(args.length >=2){
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			if(port>65535 || port <=0){
				System.out.println("Invalid port, defaulting to"+DEFAULT_SERVER_PORT);
			}
		}
		server_port = port;
		//peer_id tem que ser fixo - hash de IP e porta não serve;
		//pegar de arquivo de conf
		peer_id = Utils.getPeerID();
		String tracker = args[0];
		sendAlive(tracker);
		
		SharedFile.loadSharedFiles();	
		System.out.println("Starting Upload Server");
		UploadServer us = new UploadServer();
		Thread server = new Thread(us);
		server.start();
		if(args.length == 3){
			trackerFile = args[2];
			File f = new File(trackerFile);
			if(!f.exists()) System.out.println("Tracker file not found. Running as Uploader.");
			else{
				SharedFile dsf = TrackerUtils.parse(f);
				Downloader dl = new Downloader(dsf);
				Thread download = new Thread(dl);
				download.start();
				if(dsf.isWhole()){
					
				}
			} 
		}

	}
	
	//PUT implemented as per http://stackoverflow.com/questions/1051004 answer
	private static void sendAlive(String tracker){
		String charset = "UTF-8";
		String url = tracker+"/peers/"+peer_id;
		OutputStream output = null;
		String query = "peer_id="+peer_id+"&ip="+Utils.getExternalIP()+"&peer_port="+server_port;
		try {
			HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept-Charset", charset);
			connection.setRequestMethod("PUT");
			output = connection.getOutputStream();
			output.write(query.getBytes(charset));
		}catch(IOException e){
			e.printStackTrace();
		} finally {
		     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
		}
		
	}

}
