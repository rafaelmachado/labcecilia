package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import upload.UploadServer;
import utils.TrackerUtils;
import utils.Utils;
import download.DownloadPiece;
import download.Peer;
import download.SharedFile;

public class UploaderTestMain {

	public static String peer_id = "";
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static int server_port = DEFAULT_SERVER_PORT;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static void main(String[] args) {
		
		int port = DEFAULT_SERVER_PORT;
		server_port = port;
		peer_id = Utils.getPeerID();
		
SharedFile sh = SharedFile.makeNewSharedFile(DEFAULT_DOWNLOAD_FOLDER+"/teste.txt");
		
		File testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste.txt.tracker");
		try{
			BufferedWriter w = new BufferedWriter(new FileWriter(testfile));
			w.write(TrackerUtils.save(sh));
			w.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
		SharedFile sf = TrackerUtils.parse(testfile);
		
		
		sh = SharedFile.makeNewSharedFile(DEFAULT_DOWNLOAD_FOLDER+"/teste2.txt");
		testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste2.txt.tracker");
		try{
			BufferedWriter w = new BufferedWriter(new FileWriter(testfile));
			w.write(TrackerUtils.save(sh));
			w.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
		sf = TrackerUtils.parse(testfile);
	
		//String server = args[0];
		//sendAlive(tracker);
		SharedFile.loadSharedFiles();
		System.out.println("Starting Upload Server");
		UploadServer us = new UploadServer();
		Thread server = new Thread(us);
		server.start();

	}
	//PUT implemented as per http://stackoverflow.com/questions/1051004 answer
		private static void sendAlive(String tracker){
			String charset = "UTF-8";
			String url = tracker+"/peers/"+peer_id;
			OutputStream output = null;
			String query = "peer_id="+peer_id+"&ip="+Utils.getExternalIP()+"&peer_port="+server_port;
			try {
				HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Accept-Charset", charset);
				connection.setRequestMethod("PUT");
				output = connection.getOutputStream();
				output.write(query.getBytes(charset));
			}catch(IOException e){
				e.printStackTrace();
			} finally {
			     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
		}

}
