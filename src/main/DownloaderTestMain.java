package main;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import upload.UploadServer;
import utils.TrackerUtils;
import utils.Utils;
import download.DownloadPiece;
import download.Downloader;
import download.Peer;
import download.SharedFile;

public class DownloaderTestMain {

	public static String peer_id = "";
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static int server_port = DEFAULT_SERVER_PORT;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home");
	public static final String DEFAULT_TRACKERFILE_FOLDER = System.getProperty("user.home")+File.separator+".tracker_files";
	public static void main(String[] args) {
		
		int port = DEFAULT_SERVER_PORT;
		peer_id = Utils.getPeerID();
		server_port = port;
		File testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste.txt.tracker");
		SharedFile sf = TrackerUtils.parse(testfile);
		for(int i = 0;i<sf.getNumpieces();i++){
			sf.getPieces().get(i).setOwned(false);
		}
		sf.setFullPath(DEFAULT_DOWNLOAD_FOLDER+File.separator+sf.getName());
		Downloader d = new Downloader(sf);
		d.run();
		System.out.println(sf.calculateMD5Sum());
		System.out.println(sf.getOriginalMD5());
		System.out.println(sf.calculateMD5Sum().equals(sf.getOriginalMD5())); 
		System.out.println("done");
		
		testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste2.txt.tracker");
		sf = TrackerUtils.parse(testfile);
		for(int i = 0;i<sf.getNumpieces();i++){
			sf.getPieces().get(i).setOwned(false);
		}
		sf.setFullPath(DEFAULT_DOWNLOAD_FOLDER+File.separator+sf.getName());
		d = new Downloader(sf);
		d.run();
		System.out.println(sf.calculateMD5Sum());
		System.out.println(sf.getOriginalMD5());
		System.out.println(sf.calculateMD5Sum().equals(sf.getOriginalMD5())); 
		System.out.println("done");
	
		
	}
	//PUT implemented as per http://stackoverflow.com/questions/1051004 answer
		private static void sendAlive(String tracker){
			String charset = "UTF-8";
			String url = tracker+"/peers/"+peer_id;
			OutputStream output = null;
			String query = "peer_id="+peer_id+"&ip="+Utils.getExternalIP()+"&peer_port="+server_port;
			try {
				HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Accept-Charset", charset);
				connection.setRequestMethod("PUT");
				output = connection.getOutputStream();
				output.write(query.getBytes(charset));
			}catch(IOException e){
				e.printStackTrace();
			} finally {
			     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
		}

}
