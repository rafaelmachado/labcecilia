package download;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import main.UploaderTestMain;
import utils.TrackerUtils;
import utils.Utils;

public class SharedFile {
	static private HashMap<String,SharedFile> sharedfiles = null;
	static public int piecesize = 4096;//bytes
	private String name;
	private long length; //bytes
	private int numpieces;
	private List<DownloadPiece> pieces;
	private String checksum;
	
	private String fullpath;
	 
	
	public SharedFile(long length, String fullpath, String checksum, List<DownloadPiece> pieces){
		String[] path = fullpath.split(File.separator);
		this.name = path[path.length-1];
		this.length = length;
		this.numpieces = (int)Math.ceil((double)length/piecesize);
		this.fullpath = fullpath;
		this.pieces = pieces;
		this.checksum = checksum;
	}

	//common getters
	public String getName() {
		return name;
	}

	public long getLength() {
		return length;
	}
	
	public int getNumpieces() {
		return numpieces;
	}

	public boolean hasPiece(Integer piece){
		return pieces.get(piece).isOwned();
	}
	
	public void addPiece(Integer piece){
		pieces.get(piece).setOwned(true);
	}
	public int getPiecesize(int piece) {
		if(piece<numpieces) return SharedFile.piecesize;
		else if(piece == numpieces) return (int)(length-(numpieces-1)*piecesize);
		return -1;
	}
	
	public List<DownloadPiece> getPieces(){
		return pieces;
	}

	//file and part-related methods
	public boolean isWhole(){
		File f = new File(fullpath);
		System.out.println("fullpath from iswhole: "+fullpath);
		return f.exists();
	}
	
	public RandomAccessFile getFile(){
		try{
			RandomAccessFile raf = new RandomAccessFile(fullpath,"r");
			return raf;
		}catch(FileNotFoundException e){
			return null;
		}
	}
	
	public RandomAccessFile getPartFile(Integer part){
		String path = fullpath+"."+part.toString();
		try{
			RandomAccessFile raf = new RandomAccessFile(path,"r");
			return raf;
		}catch(FileNotFoundException e){
			return null;
		}
		
	}
		
	public boolean checkPart(Integer part) {
	//Used if there is a partial file
		String path = fullpath+"."+part.toString();
		DownloadPiece p = pieces.get(part);
		MessageDigest md = null;
		InputStream fis;
		byte[] buffer = new byte[1024];
		try {
			fis = new FileInputStream(path);
			md = MessageDigest.getInstance("MD5");
			int numRead;
		    do {
		    	numRead = fis.read(buffer);
		    	if (numRead > 0) {
		    		md.update(buffer, 0, numRead);
		    	}
		    } while (numRead != -1);
	        fis.close();
		
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if(md!=null)
			return Utils.getHexString(md.digest()).equals(p.getChecksum());
		return false;
	}
	
	//checksum-related methods
	public String calculateMD5Sum(){
		return Utils.calculateMD5Sum(fullpath);
			}
	
	public String getOriginalMD5(){
		return checksum;
	}

	//static methods
	public static SharedFile getFile(String filename) {
		return sharedfiles.get(filename);
	}
	public static void loadSharedFiles(){
				
		if(sharedfiles!=null)return;
		sharedfiles = new HashMap<String,SharedFile>();
		String home = System.getProperty("user.home"); 
		String infodir = home+"/.tracker_files";
		File dir = new File(infodir);
		if(!dir.exists() || !dir.isDirectory()) return;
		else{
			FileFilter filter = new FileFilter() {
				public boolean accept(File pathname) {
					if(pathname.getName().endsWith(".tracker"))
						return true;
					return false;
				}
			};
			for(File tracker: dir.listFiles(filter)){
				SharedFile sf = TrackerUtils.parse(tracker);
				System.out.println("loaded "+sf.getName());
				sharedfiles.put(sf.getName(),sf);
			}
		}
	}
	
	public void makeFullFile(){
		try{
			FileOutputStream fos=  new FileOutputStream(fullpath);
			FileInputStream fis;
			byte[] piece = new byte[4096];
			for(Integer i = 0; i< numpieces; i++){
				fis = new FileInputStream(fullpath+"."+i);
				fis.read(piece, 0, pieces.get(i).getSize());
				fos.write(piece, 0, pieces.get(i).getSize());
				fis.close();
				File f = new File(fullpath+"."+i);
				if(f.exists()){
					f.delete();
				}
			}
			fos.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	
	public void setFullPath(String path){
		this.fullpath = path;
	}
	public String getFullPath(){
		return this.fullpath;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public static SharedFile makeNewSharedFile(String fullpath) {
		File f = new File(fullpath);
		if(!f.exists()){
			return null;
		}
		
		Peer p = new Peer(UploaderTestMain.peer_id, Utils.getLocalIP(), UploaderTestMain.server_port);
		List<Peer> pl = new ArrayList<Peer>();
		pl.add(p);
		
		long length = f.length();
		long remaining = f.length();
		int start = 0;
		int numpieces = (int)Math.ceil((double)length/SharedFile.piecesize);
		List<DownloadPiece> dpl = new ArrayList<DownloadPiece>();
		int size;
		while(remaining >0){
			size = piecesize;
			if(remaining<piecesize)size = (int)remaining;
			String checksum = Utils.calculatePartialMD5(fullpath, start, size);
			DownloadPiece dp= new DownloadPiece(size,checksum,pl);
			dp.setOwned(true);
			dpl.add(dp);
			remaining -= size;
			start+=size;
		}	
		String file_checksum = Utils.calculateMD5Sum(fullpath);
		return new SharedFile(length,fullpath,file_checksum,dpl);
	}

}	
