package download;

public class Peer {

	private String ID;
	private String IP;
	private int port;
	public Peer(String id, String ip, int port){
		this.ID = id;
		this.IP = ip;
		this.port = port;
	}
	
	public String getID() {
		return ID;
	}
	public String getIP() {
		return IP;
	}
	public int getPort() {
		return port;
	}
	
}
