package download;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Callable;

import main.DownloaderTestMain;
import main.Main;

public class DownloadWorker implements Callable<Boolean> {

	private SharedFile sf;
	private DownloadPiece piece;
	private int pieceindex;
	private boolean downloaded;
	
	public DownloadWorker(SharedFile sf, DownloadPiece piece, int pieceindex){
		this.sf = sf;
		this.piece = piece;
		this.pieceindex = pieceindex;
		this.downloaded = false;
	}
	
	@Override
	public Boolean call(){
		while(!downloaded && piece.getPeerlist().size()>0){
			Peer p = piece.getPeerlist().get(0);
			downloaded = downloadFrom(p);
			if(!downloaded) piece.getPeerlist().remove(0);
		}
		//Do something after downloaded piece - talk to tracker, p.ex.
		return downloaded;

	}
	private boolean downloadFrom(Peer p){
		//Try to download from peer, if it works save file on Main.DEFAULT_DOWNLOAD_FOLDER
		//and return true. if not remove peer from list and return false 
		try {
			Socket s = new Socket(p.getIP(), p.getPort());
			PrintWriter out = new PrintWriter(s.getOutputStream(),true);
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			//Send Request$name$pieceindex
			out.println("Request$"+sf.getName()+"$"+pieceindex);
			String read = in.readLine();
			//Check if not ack
			if(!read.equals("Ack")){}
			out.println("Start");
			DataInputStream dis = new DataInputStream(s.getInputStream());
			boolean downloaded = receiveFile(dis);
			//todo: look at checksum
			if(downloaded){
				sf.addPiece(pieceindex);
				out.println("OK");
			}
			else out.println("Error");
			dis.close();
			in.close();
			out.close();
			s.close();
			return downloaded;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	private boolean receiveFile(DataInputStream dis) throws IOException{
		byte[] bytes = new byte[piece.getSize()];
		int read = dis.read(bytes, 0, piece.getSize());
		if(read != piece.getSize()) return false;
		String fullpath = DownloaderTestMain.DEFAULT_DOWNLOAD_FOLDER + 
				 		  File.separator + sf.getName()+"."+pieceindex;
		FileOutputStream out = new FileOutputStream(fullpath);
		out.write(bytes);
		out.close();
		if(sf.checkPart(pieceindex))return true;
		return false;
	}
}
