package upload;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.Socket;

import download.SharedFile;

public class UploadWorker implements Runnable{

	private Socket s;
	private boolean open = true;
	private PrintWriter out;
	private BufferedReader in;
	public UploadWorker(Socket s){
		this.s=s;
		try{
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(),true);
		}
		catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	@Override
	public void run() {
		String received="",filename = "",part = "";
		try {
			System.out.println("new worker");
			received = in.readLine();
			System.out.println(received);
			String[] msg = received.split("[$]");
			if(msg.length!=3){System.out.println("malformed message");}
			else{
				filename = msg[1];
				part = msg[2];
			}
			out.println("Ack");
			received = in.readLine();
			if(received.equals("Start")){
				this.sendFile(filename,part);
				received = in.readLine();
				if(received.equals("Error")){
					checkFile(filename,part);				
				}
			}
		} catch (IOException e) {	
			e.printStackTrace();
		}
		while(open){}	
	}
	private boolean checkFile(String filename, String part) {
		SharedFile s = SharedFile.getFile(filename);
		if(s.isWhole()){
			boolean correct = s.calculateMD5Sum().equals(s.getOriginalMD5());
			if(!correct){
				//TODO: send message to tracker
				//whole file
			}
			return correct;
		}
		else{
			boolean correct = s.checkPart(Integer.parseInt(part));
			if(!correct){
				//TODO: send update to tracker
				//part
			}
			return correct;
		} 
	}
	
	//assumes return of getFile or getPartFile !=null
	//TODO: fix
	private void sendFile(String filename, String part) {
		try{
		int p = Integer.parseInt(part);
		DataOutputStream dos = new DataOutputStream(s.getOutputStream());
		dos.flush();
		SharedFile sfile = SharedFile.getFile(filename);
		System.out.println(filename);
		byte[] towrite = new byte[SharedFile.piecesize];
		if(sfile.isWhole()){
			RandomAccessFile file = sfile.getFile();
			file.seek(p*SharedFile.piecesize);
			file.read(towrite,0,SharedFile.piecesize);
			
			dos.write(towrite,0,SharedFile.piecesize);
		}
		else{
			RandomAccessFile file = sfile.getPartFile(p);
			file.read(towrite,0,SharedFile.piecesize);
			dos.write(towrite,0,SharedFile.piecesize);
		}
		//out.println(p+1);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public boolean closed(){
		return !open;
	}
	public void close(){
		open = false;
		try {
			in.close();
			out.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
