package utils;

import java.io.File;

import main.UploaderTestMain;

import com.thoughtworks.xstream.XStream;

import download.DownloadPiece;
import download.Peer;
import download.SharedFile;

public class TrackerUtils {

	public static SharedFile parse(File trackerFile){
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("peer",Peer.class);
		xstream.alias("file",SharedFile.class);
		xstream.alias("piece", DownloadPiece.class);
		SharedFile sh = (SharedFile)xstream.fromXML(trackerFile);
		if(sh.getName() == null){
			String name = trackerFile.getName().replaceAll(".tracker$", "");
			sh.setName(name);
		}
		if(sh.getFullPath()==null){
			String name = trackerFile.getName().replaceAll(".tracker$", "");
			String path = UploaderTestMain.DEFAULT_DOWNLOAD_FOLDER+File.separator+name;
			sh.setFullPath(path);
		}
		return sh;
	}
	
	public static String save(SharedFile sf){
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("peer",Peer.class);
		xstream.alias("file",SharedFile.class);
		xstream.alias("piece", DownloadPiece.class);
		xstream.omitField(SharedFile.class, "piecesowned");
		xstream.omitField(SharedFile.class, "name");
		xstream.omitField(SharedFile.class, "fullpath");
		String xml = xstream.toXML(sf);
		return xml;
	}
	
	
}
