package server;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import models.Peer;
import models.Resource;
import models.TrackerFile;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import utils.ResponseUtils;


public class PeerTask {

	/*
	 * Peer is requesting an update of its ip:port on the tracker server:
	 * curl -X PUT -d "ip=127.0.0.1&port=8080" localhost:8080/peers/123
	 */
	public static class PUT implements Runnable {

		private Response response;
		private Request request;
		private String uri;
		private Resource resource;
		private String peerId;
		private Peer peer;
		private boolean found;

		public PUT(Request request, Response response) {
			this.response = response;
			this.request = request;
			this.uri = request.getPath().getPath();
			this.resource = new Resource(uri);
			this.peerId = this.resource.getPeerId();
			this.peer = Peer.get(this.peerId);
			this.found = false; 
			if (peer != null && peer.getID().equals(peerId)) found = true;
		}

		@Override
		public void run() {
			PrintStream body = null;
			try {
				body = response.getPrintStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Map<String,String> data = null;
			try {
				data = request.getForm();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ResponseUtils.setDefaultResponseHeader(response);
			if (!found) {
				create(data);
				response.setCode(201);
				System.out.println("[LOG]: Peer created with ID=" + peer.getID());
			} else {
				update(data);
				System.out.println("[LOG]: Peer updated (ID=" + peer.getID() + ")");
			}
			
			
			body.close();

		}

		private void update(Map<String, String> data) {
			peer.setIP(data.get("ip"));
			peer.setPort((Integer.parseInt(data.get("port"))));
			peer.save();
		}

		private void create(Map<String, String> data) {
			Peer p = new Peer(peerId, data.get("ip"), Integer.parseInt(data.get("port")));
			p.save();
			this.peer = p;
		}

	}

}
