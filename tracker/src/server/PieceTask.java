package server;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import models.Peer;
import models.Resource;
import models.TrackerFile;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import utils.ResponseUtils;

public class PieceTask {

	/*
	 * Peer wants to let tracker know he has just finished downloading of some piece
	 * curl -X PUT  /resource.tracker/pieces/2/peers/123 
	 */
	public static class PUT implements Runnable {

		private Response response;
		private Request request;
		private String uri;
		private Resource resource;
		private String peerId;
		private Peer peer;
		private boolean foundPeer;
		private String pieceId;
		private TrackerFile file;
		private String fileName;
		private boolean foundPiece;
		private boolean found;

		public PUT(Request request, Response response) {
			this.response = response;
			this.request = request;
			this.uri = request.getPath().getPath();
			this.resource = new Resource(uri);
			
			this.peerId = this.resource.getPeerId();
			this.peer = Peer.get(this.peerId);
			this.foundPeer = (peer != null && peer.getID().equals(peerId));
			
			this.fileName = this.resource.getFileName();
			this.file = TrackerFile.get(this.fileName);

			this.pieceId = this.resource.getPieceId();
			this.foundPiece = (this.file.hasPiece(this.pieceId));
			
			System.out.println("pieceid: " + pieceId);
			System.out.println("peerid: " + peerId);
			System.out.println("foundPeer" + this.foundPeer);
			System.out.println("foundPiece" + this.foundPiece);
			System.out.println("fileName" + this.fileName);
			
			this.found = this.foundPeer && this.foundPiece;
		}

		@Override
		public void run() {
			PrintStream body = null;
			try {
				body = response.getPrintStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Map<String,String> data = null;
			try {
				data = request.getForm();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ResponseUtils.setDefaultResponseHeader(response);
			if (!found) {
				System.out.println("[LOG]: Error retrieving peer or piece");
				body.println("Invalid resource (Error retrieving peer or piece)");
			} else {
				file.addPiece(peer, pieceId);
				peer.save();
				file.save();
			}
			
			
			body.close();

		}

		

	}


}
